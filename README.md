# Min Android Version App Wise

This repo is an attempt to list out all android application and the minimum android version that they support.

# Ecommerce Android Apps

## Flipkart Apk

Flikpart app supports `5.1+` as minimum android version. It can be downloaded from [flipkart apk](https://apkmozo.com/apps/flipkart-online-shopping-app/com.flipkart.android)

## Amazon Apk

Amazon app supports `5.0+` as minimum android version. It can be downloaded from [amazon apk](https://apkmozo.com/apps/amazon-shopping-upi-money-transfer-bill-payment/in.amazon.mShop.android.shopping)

## Indiamart Apk

Indiamart app supports as `4.1+` minimum android version. It can be downloaded from [indiamart apk](https://apkmozo.com/apps/indiamart/com.indiamart.m)

## Myntra Apk

Myntra app supports as `5.0+` minimum android version. It can be downloaded from [myntra apk](https://apkmozo.com/apps/myntra-online-shopping-app-shop-fashion-more/com.myntra.android)

## Snapdeal Apk

Snapdeal app supports `4.1+` as minimum android version. It can be downloaded from [snapdeal apk](https://apkmozo.com/apps/snapdeal-online-shopping-app/com.snapdeal.main)